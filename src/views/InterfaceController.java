package views;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.stage.DirectoryChooser;
import model.analysis.LogAnalyzer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gms.utils.graphics.fx.GUI;

import java.io.File;
import java.util.Objects;

public class InterfaceController extends GUI {

    private static final Logger log = LogManager.getLogger();

    private final static String DEFAULT_TITLE = "ProtuneLogsAnalyzer";
    private File currentDirectory;

    @FXML
    private Button analyzeButton;

    @FXML
    private TextField currentDirectoryField;

    @FXML
    private Label logsNumberLabel;

    @FXML
    private ProgressIndicator progressIndicator;

    public InterfaceController() {
        super(DEFAULT_TITLE);
    }
    public InterfaceController(String title) {
        super(title);
    }

    @Override
    protected void initialize() {
        useInternationalization(true);
    }

    @Override
    protected void paint() {
        /*Disables the analyze button at the startup*/
        analyzeButton.setDisable(true);
    }

    @FXML
    void selectDirectory(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Directory to analyze");

        /*Setting the starting point for the chooser*/
        if(Objects.isNull(currentDirectory)) {
            log.debug("no directory selected, using the defalut");
            chooser.setInitialDirectory(new File("./resources/logs/"));
        } else {
            log.debug("previous directory found: " + currentDirectory);
            chooser.setInitialDirectory(currentDirectory);
        }
        currentDirectory = chooser.showDialog(getStage());

        if(Objects.nonNull(currentDirectory)) {
            /*Enables the analyze button*/
            analyzeButton.setDisable(false);

            /*Sets the directory and updates the logs found details*/
            currentDirectoryField.setText(currentDirectory.toString());
            updateLogsDetails(currentDirectory.getAbsolutePath());
        } else {
            log.warn("no directory selected");
        }
    }

    @FXML
    private void onAnalyzeButtonPressed(ActionEvent event) {

        /*Checks if a directory has been selected*/
        if(Objects.isNull(currentDirectory)) {
            log.warn("no directory has been selected for analysis");
            Alert alert = new Alert(Alert.AlertType.WARNING, "Select the directory containing the logs to analyze");
            alert.showAndWait();
        } else {
            /*Hides the analyze button and shows a progress indicator.
            * The analysis is done in a separate thread to prevent the UI from stuttering.*/
            analyzeButton.setVisible(false);
            progressIndicator.setVisible(true);
            Task<Void> task = new Task<Void>() {

                @Override
                protected Void call() {
                    log.info("starting analysis in " + currentDirectory.toString());
                    LogAnalyzer.analyze(currentDirectory.getAbsolutePath());
                    log.info("analysis completed");
                    return null;
                }
            };

            task.setOnSucceeded(event1 -> Platform.runLater(() -> {
                analyzeButton.setVisible(true);
                progressIndicator.setVisible(false);
            }));

            task.setOnFailed(event1 -> Platform.runLater(() -> {
                analyzeButton.setVisible(true);
                progressIndicator.setVisible(false);
                Alert alert = new Alert(Alert.AlertType.ERROR, "Error during the computation");
                alert.showAndWait();
            }));

            new Thread(task).start();
        }
    }

    private void updateLogsDetails(String directory) {
        log.debug("updating logs number label");

        File newDirectory = new File(directory);
        int logsCount = Objects.requireNonNull(newDirectory.listFiles(filename -> filename.toString().toLowerCase().endsWith(".json"))).length;
        logsNumberLabel.setText(Integer.toString(logsCount));
    }
}
