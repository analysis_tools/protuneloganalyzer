import views.InterfaceController;
import javafx.application.Application;
import javafx.stage.Stage;
import org.gms.utils.graphics.fx.GUIManager;

import java.util.Locale;

public class ProtuneLogAnalyzer extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        GUIManager manager = GUIManager.getInstance();
        manager.setPrimaryStage(primaryStage);
        manager.setDefaultLocale(new Locale("en", "EN"));

        new InterfaceController().show(true);
    }
}
