package utils;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Font;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import model.Production;
import model.log.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.JFreeChart;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class PdfUtils {

    private static final Logger log = LogManager.getLogger();

    public static String createPDF(List<Production> productions) {

        log.info("inizio creazione PDF");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmss");

        try {
            /*Apro il documento*/
            Document report = new Document();
            String fileName = dateFormat.format(new Date()) + ".pdf";
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(report, fileOutputStream);

            /*Apro il documento PDF*/
            report.open();

            /*Genero il report per ogni produzione*/
            for(Production production : productions) {
                /*Aggiungo un titolo*/
                try {
                    log.info("produzione " + production.getStartDate() + ": " + production.getLogs().size() + " profili");
                    /*Analizzo ogni produzione in una nuova pagina*/
                    report.newPage();

                    log.debug("generazione tabella produzione");
                    /*Recupero la media dei profili correnti*/
                    Double currentMean = production.getBestProfile().getProduction().getThickness();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss");
                    report.add(PdfUtils.createTitle(production.getStartDate().format(formatter) + " - " + MathUtils.round(currentMean, 0) + " µm"));
                    /*Aggiungo le informazioni della produzione*/
                    report.add(PdfUtils.createSubtitle("Trigger: " + production.getTrigger()));
                    model.log.application.Production details = production.getLogs().get(0).getProduction();
                    report.add(PdfUtils.createSubtitle("Line speed: " + details.getLineSpeed() + " m/min"));
                    report.add(PdfUtils.createSubtitle("Layflat: " + details.getLayflat() + " mm"));
                    /*Creo la tabella con le statistiche della produzione*/
                    PdfPTable tableProduction = new PdfPTable(5);
                    /*Creo le righe degli headers*/
                    tableProduction.addCell(PdfUtils.createGroupCell("Durata [#]", 1));
                    tableProduction.addCell(PdfUtils.createGroupCell("Profili [#]", 1));
                    tableProduction.addCell(PdfUtils.createGroupCell("Spessore [µm]", 3));
                    Stream.of("ore", "analizzati", "SP", "min", "max").forEach(columnTitle -> {
                        tableProduction.addCell(PdfUtils.createHeaderCell(columnTitle));
                    });
                    /*Aggiungo i dati*/
                    long hours = ChronoUnit.HOURS.between(production.getStartDate(), production.getEndDate());
                    long minutes = ChronoUnit.MINUTES.between(production.getStartDate(), production.getEndDate()) - 60 * hours;
                    tableProduction.addCell(PdfUtils.createDataCell(hours + "h " + minutes + "m"));
                    tableProduction.addCell(PdfUtils.createDataCell(production.getLogs().size()));
                    tableProduction.addCell(PdfUtils.createDataCell(currentMean));
                    tableProduction.addCell(PdfUtils.createDataCell(production.getThicknessMin()));
                    tableProduction.addCell(PdfUtils.createDataCell(production.getThicknessMax()));
                    /*Imposto uno spacing per la tabella*/
                    tableProduction.setSpacingBefore(20.0F);
                    report.add(tableProduction);

                    log.debug("generazione tabella statistiche");
                    /*Creo la tabella con le analisi sulle prestazioni*/
                    PdfPTable tableAnalysis = new PdfPTable(7);
                    /*Creo le righe degli headers*/
                    tableAnalysis.addCell(PdfUtils.createGroupCell("2Sigma [%]", 6));
                    tableAnalysis.addCell(PdfUtils.createGroupCell("Tempo [min]", 1));
                    Stream.of("start", "min", "max", "mean", "15 mins", "30 mins", "dimezzamento").forEach(columnTitle -> {
                        tableAnalysis.addCell(PdfUtils.createHeaderCell(columnTitle));
                    });
                    tableAnalysis.addCell(PdfUtils.createDataCell(production.getLogs().get(0).getStats().getSigmaPerc()));
                    tableAnalysis.addCell(PdfUtils.createDataCell(production.getBestProfile().getStats().getSigmaPerc()));
                    tableAnalysis.addCell(PdfUtils.createDataCell(production.getWorstProfile().getStats().getSigmaPerc()));
                    tableAnalysis.addCell(PdfUtils.createDataCell(production.getTwoSigmaAverage()));
                    tableAnalysis.addCell(PdfUtils.createDataCell(MathUtils.get2SigmaPercAfterTime(production, 15)));
                    tableAnalysis.addCell(PdfUtils.createDataCell(MathUtils.get2SigmaPercAfterTime(production, 30)));
                    tableAnalysis.addCell(PdfUtils.createDataCell(MathUtils.getTimeToHalve2Sigma(production)));
                    /*Imposto uno spacing per la tabella*/
                    tableAnalysis.setSpacingBefore(15.0F);
                    tableAnalysis.setSpacingAfter(5.0F);
                    report.add(tableAnalysis);

                    log.debug("generazione grafico");
                    /*Creo i grafici */
                    Log firstLog = production.getLogs().get(0);
                    /*Aggiungo i grafici al PDF*/
                    JFreeChart chart = FilesUtility.createGraphImage(firstLog.getZonesProfile(), production.getBestProfile().getZonesProfile(), production.getWorstProfile().getZonesProfile());
                    PdfContentByte content = writer.getDirectContent();
                    PdfTemplate template = content.createTemplate(475,300);
                    Graphics2D graphics = template.createGraphics(template.getWidth(), template.getHeight(), new DefaultFontMapper());
                    Rectangle2D rectangle = new Rectangle2D.Double(0, 0, template.getWidth(), template.getHeight());
                    chart.draw(graphics, rectangle);
                    graphics.dispose();
                    content.addTemplate(template, 50, 200);
                } catch (DocumentException e) {
                    log.error("error adding a title", e);
                }
            }
            report.close();
            return fileName;
        } catch (FileNotFoundException e) {
            log.error("file not found", e);
        } catch (DocumentException e) {
            log.error("error writing the pdf document", e);
        }
        return null;
    }

    private static float calculatePaddingTop(Font font, Float padding) {
        Float size = font.getSize();
        Float capHeight = font.getBaseFont().getFontDescriptor(BaseFont.CAPHEIGHT, size);
        return capHeight - size + padding;
    }

    private static void alignCell(PdfPCell cell, Font font, Float padding) {
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(padding);
        cell.setPaddingTop(PdfUtils.calculatePaddingTop(font, padding));
    }

    private static PdfPCell createGroupCell(String string, int colSpan) {
        PdfPCell cell = new PdfPCell();
        /*Stile del carattere e cella*/
        Phrase phrase = new Phrase(string, FontFactory.getFont(FontFactory.COURIER, Font.DEFAULTSIZE, Font.BOLD));
        cell.setPhrase(phrase);
        /*Stile della cella*/
        cell.setBorderWidth(2);
        cell.setColspan(colSpan);
        cell.setBackgroundColor(BaseColor.RED);
        PdfUtils.alignCell(cell, phrase.getFont(), 5F);
        return cell;
    }

    private static PdfPCell createHeaderCell(String string) {
        PdfPCell cell = new PdfPCell();
        /*Stile del carattere e cella*/
        Phrase phrase = new Phrase(string, FontFactory.getFont(FontFactory.COURIER, Font.DEFAULTSIZE, Font.BOLD));
        cell.setPhrase(phrase);
        /*Stile della cella*/
        cell.setBorderWidth(1.5F);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        PdfUtils.alignCell(cell, phrase.getFont(), 5F);
        return cell;
    }

    private static PdfPCell createDataCell(String data) {
        PdfPCell cell = new PdfPCell();
        Font font = FontFactory.getFont(FontFactory.COURIER, 12);
        PdfUtils.alignCell(cell, font, 5F);
        Phrase phrase;
        if(data != null) {
            phrase = new Phrase(data, font);
        } else {
            phrase = new Phrase("-", font);
        }
        cell.setPhrase(phrase);
        return cell;
    }

    private static PdfPCell createDataCell(Double data) {
        PdfPCell cell = new PdfPCell();
        Font font = FontFactory.getFont(FontFactory.COURIER, 12);
        PdfUtils.alignCell(cell, font, 5F);
        NumberFormat decimalFormat = new DecimalFormat("#0.00");
        Phrase phrase;
        if(data != null) {
            phrase = new Phrase(decimalFormat.format(data), font);
        } else {
            phrase = new Phrase("-", font);
        }
        cell.setPhrase(phrase);
        return cell;
    }

    private static PdfPCell createDataCell(Number data) {
        PdfPCell cell = new PdfPCell();
        Font font = FontFactory.getFont(FontFactory.COURIER, 12);
        PdfUtils.alignCell(cell, font, 5F);
        NumberFormat decimalFormat = new DecimalFormat("#0");
        Phrase phrase;
        if(data != null) {
            phrase = new Phrase(decimalFormat.format(data), font);
        } else {
            phrase = new Phrase("-", font);
        }
        cell.setPhrase(phrase);
        return cell;
    }

    private static Paragraph createTitle(String title) {
        Font font = FontFactory.getFont(FontFactory.COURIER, 24, Font.BOLD);
        Paragraph paragraph = new Paragraph(new Phrase(title, font));
        paragraph.setAlignment(Element.ALIGN_CENTER);
        return paragraph;
    }

    private static Paragraph createSubtitle(String subtitle) {
        Font font = FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD);
        Paragraph paragraph = new Paragraph(new Phrase(subtitle, font));
        paragraph.setIndentationLeft(55f);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        return paragraph;
    }
}
