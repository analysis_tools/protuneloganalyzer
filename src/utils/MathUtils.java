package utils;

import model.Production;
import model.log.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class MathUtils {

    private static final Logger log = LogManager.getLogger();

    public static Double getMax(List<Double> values) {
        return Collections.max(values);
    }

    public static Double getMean(List<Double> values) {
        Double sum = 0D;
        for (Double value : values)
            sum += value;
        return sum / values.size();
    }

    public static Double get2SigmaPercAfterTime(Production production, Integer minutes) {
        LocalDateTime startingDate = production.getStartDate();
        for(Log log : production.getLogs()) {
            if(ChronoUnit.MINUTES.between(startingDate, log.getTimeStamp()) >= minutes)
                return log.getStats().getSigmaPerc();
        }
        return null;
    }

    public static Long getTimeToHalve2Sigma(Production production) {
        LocalDateTime startingDate = production.getStartDate();
        double startingTwoSigma = production.getLogs().get(0).getStats().getSigmaPerc();
        for(Log log : production.getLogs()) {
            if(log.getStats().getSigmaPerc() <= startingTwoSigma / 2) {
                return ChronoUnit.MINUTES.between(startingDate, log.getTimeStamp());
            }
        }
        return null;
    }

    public static double round(Double value, int places) {

        /*Verifico i parametri in ingresso*/
        if (Objects.isNull(value) || places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }
}
