package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;

public class FilesUtility {

    private static final Logger log = LogManager.getLogger();

    static JFreeChart createGraphImage(double[] first, double[] best, double[] worst) {
        /*Creo il dataset da graficare*/
        XYSeries firstSerie = new XYSeries("Partenza");
        XYSeries bestSerie = new XYSeries("Migliore");
        XYSeries worstSerie = new XYSeries("Peggiore");
        for(int i = 0; i < first.length; i++) {
            firstSerie.add(i, first[i]);
            bestSerie.add(i, best[i]);
            worstSerie.add(i, worst[i]);
        }
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(firstSerie);
        dataset.addSeries(bestSerie);
        dataset.addSeries(worstSerie);
        /*Genero il grafico e lo personalizzo*/
        JFreeChart lineChart = ChartFactory.createXYLineChart(null, "Zones", "Thickness", dataset, PlotOrientation.VERTICAL, true, false, false);
        lineChart.setAntiAlias(true);
        lineChart.setTextAntiAlias(true);
        lineChart.setBorderPaint(Color.darkGray);
        XYPlot plot = lineChart.getXYPlot();
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for(int i = 0; i < dataset.getSeriesCount(); i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
        }
        /*Imposto la linea tratteggiata per la prima serie (quella di partenza)*/
        renderer.setSeriesStroke(0,
                new BasicStroke(
                        1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                        1.0f, new float[] {6.0f, 6.0f}, 0.0f
                ));
        plot.setRenderer(renderer);

        /*Sistemo il range di visualizzazione dei dati*/
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        double boundsRange = (rangeAxis.getRange().getCentralValue() - rangeAxis.getRange().getLowerBound()) * 2;
        rangeAxis.setRange(rangeAxis.getRange().getCentralValue() - boundsRange, rangeAxis.getRange().getCentralValue() + boundsRange);
        return lineChart;
    }
}
