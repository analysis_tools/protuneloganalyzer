package model.analysis;

import model.Production;
import model.log.Logs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.PdfUtils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class LogAnalyzer {

    private static final Logger log = LogManager.getLogger();

    public static void analyze(String path) {

        /*Importing the logs*/
        Logs logs = new Logs(path);

        /*Splitting the logs into different productions*/
        List<Production> productions = Logs.splitIntoProductions(logs.getLogs());

        /*Creating the PDF report*/
        String fileName = PdfUtils.createPDF(productions);

        /*Opening the PDF file*/
        try {
            Desktop.getDesktop().open(new File(fileName));
        } catch (IOException e) {
            log.error("error opening the pdf file", e);
        }
    }
}