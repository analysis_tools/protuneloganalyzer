package model.log.devices;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hauloff {
    private boolean direction;
    private int position;
    private int speed;
    private int last_period;

    @JsonProperty("direction")
    public boolean getDirection() {
        return direction;
    }

    @JsonProperty("direction")
    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    @JsonProperty("position")
    public int getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(int position) {
        this.position = position;
    }

    @JsonProperty("speed")
    public int getSpeed() {
        return speed;
    }

    @JsonProperty("speed")
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @JsonProperty("last_period")
    public int getLastPeriod() {
        return last_period;
    }

    @JsonProperty("last_period")
    public void setLastPeriod(int last_period) {
        this.last_period = last_period;
    }
}
