package model.log.devices;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Actuators {
    private int[] values;
    private boolean[] forced;

    @JsonProperty("values")
    public int[] getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(int[] values) {
        this.values = values;
    }

    @JsonProperty("forced")
    public boolean[] getForced() {
        return forced;
    }

    @JsonProperty("forced")
    public void setForced(boolean[] forced) {
        this.forced = forced;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Actuators actuators = (Actuators)o;
        return Arrays.equals(this.forced, actuators.forced) && Arrays.equals(this.values, actuators.values);
    }

    public List<Integer> getForcedActuators() {
        List<Integer> forcedActuators = new ArrayList<>();
        for(int i = 0; i < forced.length; i++) {
            if(forced[i])
                forcedActuators.add(i);
        }

        Collections.sort(forcedActuators);

        return forcedActuators;
    }
}