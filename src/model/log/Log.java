package model.log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.log.application.Parameters;
import model.log.application.Production;
import model.log.application.Stats;
import model.log.application.Status;
import model.log.devices.Actuators;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Log {
    private Parameters parameters;
    private Production production;
    private Status status;
    private Stats stats;
    private double[] rawProfile;
    private double[] zonesProfile;
    private Actuators actuators;

    private LocalDateTime lastModified;
    private LocalDateTime timeStamp;

    @JsonProperty("parameters")
    public Parameters getParameters() {
        return parameters;
    }

    @JsonProperty("parameters")
    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    @JsonProperty("production")
    public Production getProduction() {
        return production;
    }

    @JsonProperty("production")
    public void setProduction(Production production) {
        this.production = production;
    }

    @JsonProperty("status")
    public Status getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonProperty("stats")
    public Stats getStats() {
        return stats;
    }

    @JsonProperty("stats")
    public void setStats(Stats stats) {
        this.stats = stats;
    }

    @JsonProperty("rawProfile")
    public double[] getRawProfile() {
        return rawProfile;
    }

    @JsonProperty("rawProfile")
    public void setRawProfile(double[] rawProfile) {
        this.rawProfile = rawProfile;
    }

    @JsonProperty("zonesProfile")
    public double[] getZonesProfile() {
        return zonesProfile;
    }

    @JsonProperty("zonesProfile")
    public void setZonesProfile(double[] zonesProfile) {
        this.zonesProfile = zonesProfile;
    }

    @JsonProperty("actuators")
    public Actuators getActuators() {
        return actuators;
    }

    @JsonProperty("actuators")
    public void setActuators(Actuators actuators) {
        this.actuators = actuators;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public LocalDateTime getLastModified() { return lastModified; }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public LocalDateTime getTimeStamp() { return timeStamp; }

    static Log readJSON(File log) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(log, Log.class);
    }
}

