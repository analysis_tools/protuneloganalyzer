package model.log;

import com.google.common.base.Stopwatch;
import javafx.scene.paint.Stop;
import model.Production;
import model.log.application.Stats;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Logs {

    private static final Logger log = LogManager.getLogger();

    private List<Log> logs;

    public Logs() {
        this.logs = new LinkedList<>();
    }

    public Logs(String path) {
        this.logs = Logs.loadLogsFromPath(path);
    }

    public List<Log> getLogs() {
        return logs;
    }

    public static List<Log> loadLogsFromPath(String path) {

        Path directoryPath = Paths.get(path);
        Stopwatch stopwatch = Stopwatch.createUnstarted();

        if(!Files.isDirectory(directoryPath)) {
            log.error("the provided path is not a directory: " + path, new FileNotFoundException());
        }

        File directory = directoryPath.toFile();
        File[] logs = directory.listFiles((dir, name) -> name.toLowerCase().endsWith(".json"));

        if(Objects.isNull(logs)) {
            log.warn("no log file found in the provided path: " + path);
            return Collections.emptyList();
        }

        /*Sort the files*/
        log.info("sorting the files in temporal order");
        stopwatch.start();
        Arrays.sort(logs, Comparator.comparingLong(File::lastModified));
        stopwatch.stop();
        log.debug("time to sort in temporal order: " + stopwatch);

        log.info("found " + logs.length + " logs");
        List<Log> logList = new LinkedList<>();

        for(File file : logs) {
            log.trace("loading " + file);
            try {
                Log currentLog = Log.readJSON(file);
                LocalDateTime dateTime = Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                currentLog.setLastModified(dateTime);
                StringBuilder timeStampToParse = new StringBuilder(file.getName().replaceAll("[^\\d]", ""));
                while(timeStampToParse.length() < 13)
                    timeStampToParse.append("0");
                dateTime = Instant.ofEpochMilli(Long.parseLong(timeStampToParse.toString()))
                                .atZone(ZoneId.systemDefault()).toLocalDateTime();
                currentLog.setTimeStamp(dateTime);
                logList.add(currentLog);
            } catch (IOException e) {
                log.error("error parsing the JSON files", e);
            }
        }

        return logList;
    }

    public static List<Production> splitIntoProductions(List<Log> logs) {

        LinkedList<Production> productions = new LinkedList<>();
        Stopwatch stopwatch = Stopwatch.createUnstarted();

        String trigger = "none";
        Log previousLog = null;
        Log bestProfile = null;
        Log worstProfile = null;
        double bestSigma = Double.MAX_VALUE;
        double worstSigma = Double.MIN_VALUE;
        LocalDateTime startDate = null;
        LocalDateTime endDate = null;
        LinkedList<Log> currentList = new LinkedList<>();
        int logsWithActuationActive = 0;
        int productionInterruptions = 0;

        stopwatch.start();
        for(Log currentLog : logs) {
            /*Splitting the productions based on the production details and the actuation reset*/
            if (previousLog == null ||
                    !currentLog.getProduction().equals(previousLog.getProduction()) ||
                    currentLog.getStatus().getApplication().getActuationActive() != previousLog.getStatus().getApplication().getActuationActive() ||
                    (!currentLog.getStatus().getApplication().getActuationActive() && !currentLog.getActuators().equals(previousLog.getActuators()))) {
                log.trace("found a production interruption");
                /*Creating a new list only if the previous is not empty*/
                if(!currentList.isEmpty()) {
                    log.trace("production log list not empty, creating a new one");

                    /*Adding last production found*/
                    productions.add(new Production(trigger, currentList, startDate, endDate, bestProfile, worstProfile));

                    if(!currentLog.getProduction().equals(previousLog.getProduction()))
                        trigger = "production details changed";
                    else if(currentLog.getStatus().getApplication().getActuationActive() !=
                            previousLog.getStatus().getApplication().getActuationActive()) {
                        if (currentLog.getStatus().getApplication().getActuationActive())
                            trigger = "started regulation";
                        else
                            trigger = "stopped regulation";
                    } else if(!currentLog.getStatus().getApplication().getActuationActive() && !currentLog.getActuators().equals(previousLog.getActuators()))
                        trigger = "actuators changed manually";

                    /*Creating the new list for next production*/
                    currentList = new LinkedList<>();

                    /*Resetting the values*/
                    bestProfile = null;
                    worstProfile = null;
                    bestSigma = Double.MAX_VALUE;
                    worstSigma = Double.MIN_VALUE;
                    startDate = null;
                    endDate = null;

                    productionInterruptions++;
                } else {
                    log.trace("production log list empty, keeping it");
                }
            }

            /*Adding the log to the current list*/
            currentList.add(currentLog);

            /*Setting the start date, if it's a new production*/
            if(Objects.isNull(startDate))
                startDate = currentLog.getTimeStamp();

            /*Checking the sigma values*/
            Stats currentStats = currentLog.getStats();
            if(bestSigma > currentStats.getSigmaPerc()) {
                bestSigma = currentStats.getSigmaPerc();
                bestProfile = currentLog;
            }
            if(worstSigma < currentStats.getSigmaPerc()) {
                worstSigma = currentStats.getSigmaPerc();
                worstProfile = currentLog;
            }

            /*The end date is always updated*/
            endDate = currentLog.getTimeStamp();

            previousLog = currentLog;
        }

        /*Adding last list*/
        if(!currentList.isEmpty()) {
            productions.add(new Production(trigger, currentList, startDate, endDate, bestProfile, worstProfile));
        }
        stopwatch.stop();
        log.debug("time to split into productions: " + stopwatch);

        log.info("production interruptions found: " + productionInterruptions);
        log.info("total logs: " + logs.size() + ", logs with actuation active: " + logsWithActuationActive);

        return productions;
    }
}
