package model.log.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.log.devices.Hauloff;
import model.log.devices.Measurer;

public class Status {
    private Application application;
    private Measurer measurer;
    private Hauloff hauloff;

    @JsonProperty("application")
    public Application getApplication() {
        return application;
    }

    @JsonProperty("application")
    public void setApplication(Application application) {
        this.application = application;
    }

    @JsonProperty("measurer")
    public Measurer getMeasurer() {
        return measurer;
    }

    @JsonProperty("measurer")
    public void setMeasurer(Measurer measurer) {
        this.measurer = measurer;
    }

    @JsonProperty("hauloff")
    public Hauloff getHauloff() {
        return hauloff;
    }

    @JsonProperty("hauloff")
    public void setHauloff(Hauloff hauloff) {
        this.hauloff = hauloff;
    }
}
