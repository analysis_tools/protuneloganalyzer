package model.log.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameters {
    private int integral_band;
    private int dead_band;
    private double small_gain;
    private double large_gain;
    private int valves_min;
    private int valves_max;
    private int matrix_minor;
    private int matrix_major;
    private int profiles_for_regulation;
    private int phase_shift_misDx_haulDx;
    private int phase_shift_misDx_haulSx;
    private int phase_shift_misSx_haulDx;
    private int phase_shift_misSx_haulSx;
    private int disturbation_type;
    private double disturbation_amount;
    private double disturbation_amplitude;
    private int disturbation_cycles;
    private int disturbation_frequency;

    @JsonProperty("integral_band")
    public int getIntegralBand() {
        return integral_band;
    }

    @JsonProperty("integral_band")
    public void setIntegralBand(int integral_band) {
        this.integral_band = integral_band;
    }

    @JsonProperty("dead_band")
    public int getDeadBand() {
        return dead_band;
    }

    @JsonProperty("dead_band")
    public void setDeadBand(int dead_band) {
        this.dead_band = dead_band;
    }

    @JsonProperty("small_gain")
    public double getSmallGain() {
        return small_gain;
    }

    @JsonProperty("small_gain")
    public void setSmallGain(double small_gain) {
        this.small_gain = small_gain;
    }

    @JsonProperty("large_gain")
    public double getLargeGain() {
        return large_gain;
    }

    @JsonProperty("large_gain")
    public void setLargeGain(double large_gain) {
        this.large_gain = large_gain;
    }

    @JsonProperty("valves_min")
    public int getValvesMin() {
        return valves_min;
    }

    @JsonProperty("valves_min")
    public void setValvesMin(int valves_min) {
        this.valves_min = valves_min;
    }

    @JsonProperty("valves_max")
    public int getValvesMax() {
        return valves_max;
    }

    @JsonProperty("valves_max")
    public void setValvesMax(int valves_max) {
        this.valves_max = valves_max;
    }

    @JsonProperty("matrix_minor")
    public int getMatrixMinor() {
        return matrix_minor;
    }

    @JsonProperty("matrix_minor")
    public void setMatrixMinor(int matrix_minor) {
        this.matrix_minor = matrix_minor;
    }

    @JsonProperty("matrix_major")
    public int getMatrixMajor() {
        return matrix_major;
    }

    @JsonProperty("matrix_major")
    public void setMatrixMajor(int matrix_major) {
        this.matrix_major = matrix_major;
    }

    @JsonProperty("profiles_for_regulation")
    public int getProfilesForRegulation() {
        return profiles_for_regulation;
    }

    @JsonProperty("profiles_for_regulation")
    public void setProfilesForRegulation(int profiles_for_regulation) {
        this.profiles_for_regulation = profiles_for_regulation;
    }

    @JsonProperty("phase_shift_misDx_haulDx")
    public int getPhaseShiftMisDxHaulDx() {
        return phase_shift_misDx_haulDx;
    }

    @JsonProperty("phase_shift_misDx_haulDx")
    public void setPhaseShiftMisDxHaulDx(int phase_shift_misDx_haulDx) {
        this.phase_shift_misDx_haulDx = phase_shift_misDx_haulDx;
    }

    @JsonProperty("phase_shift_misDx_haulSx")
    public int getPhaseShiftMisDxHaulSx() {
        return phase_shift_misDx_haulSx;
    }

    @JsonProperty("phase_shift_misDx_haulSx")
    public void setPhaseShiftMisDxHaulSx(int phase_shift_misDx_haulSx) {
        this.phase_shift_misDx_haulSx = phase_shift_misDx_haulSx;
    }

    @JsonProperty("phase_shift_misSx_haulDx")
    public int getPhaseShiftMisSxHaulDx() {
        return phase_shift_misSx_haulDx;
    }

    @JsonProperty("phase_shift_misSx_haulDx")
    public void setPhaseShiftMisSxHaulDx(int phase_shift_misSx_haulDx) {
        this.phase_shift_misSx_haulDx = phase_shift_misSx_haulDx;
    }

    @JsonProperty("phase_shift_misSx_haulSx")
    public int getPhaseShiftMisSxHaulSx() {
        return phase_shift_misSx_haulSx;
    }

    @JsonProperty("phase_shift_misSx_haulSx")
    public void setPhaseShiftMisSxHaulSx(int phase_shift_misSx_haulSx) {
        this.phase_shift_misSx_haulSx = phase_shift_misSx_haulSx;
    }

    @JsonProperty("disturbation_type")
    public int getDisturbationType() {
        return disturbation_type;
    }

    @JsonProperty("disturbation_type")
    public void setDisturbationType(int disturbation_type) {
        this.disturbation_type = disturbation_type;
    }

    @JsonProperty("disturbation_amount")
    public double getDisturbationAmount() {
        return disturbation_amount;
    }

    @JsonProperty("disturbation_amount")
    public void setDisturbationAmount(double disturbation_amount) {
        this.disturbation_amount = disturbation_amount;
    }

    @JsonProperty("disturbation_amplitude")
    public double getDisturbationAmplitude() {
        return disturbation_amplitude;
    }

    @JsonProperty("disturbation_amplitude")
    public void setDisturbationAmplitude(double disturbation_amplitude) {
        this.disturbation_amplitude = disturbation_amplitude;
    }

    @JsonProperty("disturbation_cycles")
    public int getDisturbationCycles() {
        return disturbation_cycles;
    }

    @JsonProperty("disturbation_cycles")
    public void setDisturbationCycles(int disturbation_cycles) {
        this.disturbation_cycles = disturbation_cycles;
    }

    @JsonProperty("disturbation_frequency")
    public int getDisturbationFrequency() {
        return disturbation_frequency;
    }

    @JsonProperty("disturbation_frequency")
    public void setDisturbationFrequency(int disturbation_frequency) {
        this.disturbation_frequency = disturbation_frequency;
    }
}