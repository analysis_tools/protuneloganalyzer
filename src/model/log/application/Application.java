package model.log.application;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Application {
    private boolean measurer_active;
    private boolean actuation_active;

    @JsonProperty("measurer_active")
    public boolean getMeasurerActive() {
        return measurer_active;
    }

    @JsonProperty("measurer_active")
    public void setMeasurerActive(boolean measurer_active) {
        this.measurer_active = measurer_active;
    }

    @JsonProperty("actuation_active")
    public boolean getActuationActive() {
        return actuation_active;
    }

    @JsonProperty("actuation_active")
    public void setActuationActive(boolean actuation_active) {
        this.actuation_active = actuation_active;
    }
}
