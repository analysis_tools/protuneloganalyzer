package model.log.application;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Production {
    private double line_speed;
    private double layflat;
    private double thickness;

    @JsonProperty("line_speed")
    public double getLineSpeed() {
        return line_speed;
    }

    @JsonProperty("line_speed")
    public void setLineSpeed(double line_speed) {
        this.line_speed = line_speed;
    }

    @JsonProperty("layflat")
    public double getLayflat() {
        return layflat;
    }

    @JsonProperty("layflat")
    public void setLayflat(double layflat) {
        this.layflat = layflat;
    }

    @JsonProperty("thickness")
    public double getThickness() {
        return thickness;
    }

    @JsonProperty("thickness")
    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Production production = (Production) o;
        return layflat == production.layflat &&
                thickness == production.thickness &&
                line_speed == production.line_speed;
    }
}
