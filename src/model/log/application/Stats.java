package model.log.application;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stats {
    private double min_thickness;
    private double max_thickness;
    private double mean_thickness;
    private double sigma_perc;
    private double sigma_abs;
    private double stdDev;

    public Stats() {
        this.min_thickness = Double.MIN_VALUE;
        this.max_thickness = Double.MAX_VALUE;
        this.mean_thickness = 0;
        this.sigma_perc = 0;
        this.sigma_abs = 0;
        this.stdDev = 0;
    }

    @JsonProperty("min_thickness")
    public double getMinThickness() {
        return min_thickness;
    }

    @JsonProperty("min_thickness")
    public void setMinThickness(double min_thickness) {
        this.min_thickness = min_thickness;
    }

    @JsonProperty("max_thickness")
    public double getMaxThickness() {
        return max_thickness;
    }

    @JsonProperty("max_thickness")
    public void setMaxThickness(double max_thickness) {
        this.max_thickness = max_thickness;
    }

    @JsonProperty("mean_thickness")
    public double getMeanThickness() {
        return mean_thickness;
    }

    @JsonProperty("mean_thickness")
    public void setMeanThickness(double mean_thickness) {
        this.mean_thickness = mean_thickness;
    }

    @JsonProperty("sigma_perc")
    public double getSigmaPerc() {
        return sigma_perc;
    }

    @JsonProperty("sigma_perc")
    public void setSigmaPerc(double sigma_perc) {
        this.sigma_perc = sigma_perc;
    }

    @JsonProperty("sigma_abs")
    public double getSigmaAbs() {
        return sigma_abs;
    }

    @JsonProperty("sigma_abs")
    public void setSigmaAbs(double sigma_abs) {
        this.sigma_abs = sigma_abs;
    }

    @JsonProperty("stdDev")
    public double getStdDev() {
        return stdDev;
    }

    @JsonProperty("stdDev")
    public void setStdDev(double stdDev) {
        this.stdDev = stdDev;
    }
}
