package model;

import model.log.Log;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class Production {

    private String trigger;
    private List<Log> logs;
    private Log bestProfile;
    private Log worstProfile;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    public Production(String trigger, List<Log> logs, LocalDateTime startDate, LocalDateTime endDate, Log bestProfile, Log worstProfile) {
        this.trigger = trigger;
        this.logs = logs;
        this.startDate = startDate;
        this.endDate = endDate;
        this.bestProfile = bestProfile;
        this.worstProfile = worstProfile;
    }

    public String getTrigger() {
        return trigger;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public Log getBestProfile() {
        return bestProfile;
    }

    public Log getWorstProfile() {
        return worstProfile;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Double getTwoSigmaAverage() {
        double sum = 0D;
        for(Log log : logs) {
            sum += log.getStats().getSigmaPerc();
        }
        return sum / logs.size();
    }

    public Double getThicknessMax() {
        double max = Double.MIN_VALUE;
        for(Log log : logs) {
            double logMax = Arrays.stream(log.getZonesProfile()).max().orElse(0D);
            if(max < logMax)
                max = logMax;
        }
        return max;
    }

    public Double getThicknessMin() {
        double min = Double.MAX_VALUE;
        for(Log log : logs) {
            double logMin = Arrays.stream(log.getZonesProfile()).min().orElse(0D);
            if(min > logMin)
                min = logMin;
        }
        return min;
    }
}
